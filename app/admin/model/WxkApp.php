<?php
/**
 * 企业微信自建应用
 * Created by PhpStorm.
 * User: 万奇
 * Date: 2021/12/24 17:45
 * Time: 17:45
 */

namespace app\admin\model;


use app\core\Wechat;
use think\facade\Db;

class WxkApp extends BasicModel
{
    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    /**
     * APP消息放送
     * User: 万奇
     * Date: 2021/12/24 18:36
     * @param $staff_user -成员ID,多个 '|' 分隔
     * @param $msg - 消息内容
     */
    public static function app_msg_send($staff_user, $msg){
        $wechat             = new Wechat();
        $url                = 'https://qyapi.weixin.qq.com/cgi-bin/message/send';
        $agent_id           = Db::name('wxk_config')->where(true)->value('wxk_app_agent_id');
        $data               = ['touser' => $staff_user, 'msgtype' => 'text', 'agentid' => $agent_id, 'text' => ['content' => $msg]];

        $wechat->request_wechat_api($url, 'wxk_app_secret', $data, true, true);
    }

}