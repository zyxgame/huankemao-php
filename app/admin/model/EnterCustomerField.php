<?php
/**
 * User: 万奇
 * Date: 2021/12/3 17:26
 */

namespace app\admin\model;


use think\facade\Cache;

class EnterCustomerField extends BasicModel
{
    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    /**
     * 字段管理列表
     * User: 万奇
     * Date: 2021/12/3 18:24
     * @param $param
     * @return mixed
     */
    public function enter_customer_field_list($param){
        $list       = array_grouping($this->column('id,name,pid'), 'pid');

        $list       = category_group($list, $list[0], 'group');

        return $list;
    }

    /**
     * 新增编辑删除客户字段管理
     * User: 万奇
     * Date: 2021/12/3 17:32
     * @param $param
     */
    public function add_enter_customer_field($param){
        if (is_exists($param['del'])){
            // 删除
            $this->where(['id' => $param['id']])->delete();
        }elseif (is_exists($param['id'])){
            // 编辑
            $this->where(['id' => $param['id']])->update(['name' => $param['name']]);
        }else{
            // 新增
            $this->insert(['pid' => $param['pid'], 'name' => $param['name']]);
        }

        $list   = array_grouping($this->column('id,pid,name'), 'pid');

        Cache::set('customer_field', json_encode($list));
    }

    /**
     * 获取客户业务字段
     * User: 万奇
     * Date: 2021/12/3 17:32
     * @param $name
     * @return array
     */
    public function get_customer_field($name){
        $list       = json_decode(Cache::get('customer_field'), true);

        if (!$list){
            $list   = array_grouping($this->column('id,pid,name'), 'pid');

            Cache::set('customer_field', json_encode($list));
        }

        return array_column($list[array_column($list[0], 'id', 'name')[$name]], 'name', 'id');
    }

}