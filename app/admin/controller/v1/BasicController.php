<?php
/**
 * Created by PhpStorm.
 * User: 万奇
 * Date: 2020/11/19 0019
 * Time: 18:02
 */

namespace app\admin\controller\v1;


use app\core\BaseController;
use think\App;
use think\facade\Cache;
use think\facade\Db;

class BasicController extends BaseController
{
    protected $param; // 保存请求的参数

    protected $user_info; // 用户信息

    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->set_user_info($this->_set_user_relative_param()); // 处理参数和用户信息
        $this->_user_auth(); // 用户鉴权
    }

    /**
     * 设置获取用户相关的参数
     * User: 万奇
     * Date: 2020/9/3 0003
     * @return array
     */
    private function _set_user_relative_param(){
        return [ 'user_id' , 'time', 'token', 'sign'];
    }

    /**
     * 设置用户信息
     * User: 万奇
     * Date: 2020/9/3 0003
     * @param array $handle_param 需要处理的参数
     */
    protected function set_user_info($handle_param = []){
        $post_param         = input('post.');

        if(!empty($handle_param)){
            foreach($handle_param as $hk => $hv){
                $this->user_info[$hv]   = isset($post_param[$hv]) ? $post_param[$hv] : '';
            }
        }

        $this->param        = array_del_key($post_param , $handle_param, false);
    }

    /**
     * 用户鉴权
     * User: 万奇
     * Date: 2020/9/3 0003
     */
    protected function _user_auth(){
        $user           = Db::name('sys_user')->where(['id' => $this->user_info['user_id']])->find();
        if (!$user){
            response(500, '用户不存在');
        }

        if ($user['is_main'] != 1){
            $role_module_list       = Cache::get('role_module_list_' . $user['role_id']);

            if (!$role_module_list){
                response(501, '该用户角色被禁用');
            }

            if ($user['disable'] == 1){
                response(501, '该账号已被禁用');
            }

            $module_list            = Cache::get('module_list') ?: $this->get_module_list();

            if (isset($module_list[$this->request->pathinfo()])){
                if (!isset($role_module_list[$this->request->pathinfo()])){
                    response(502, '没有权限');
                }
            }
        }

        $this->user_info['role_id'] = $user['role_id'];
    }

    /**
     * 获取权限列表
     * User: 万奇
     * Date: 2021/2/6 0006
     * @return array
     */
    protected function get_module_list(){
        $module_list            = Db::name('sys_module')->where(['is_menu' => 0])->column('url', 'url');
        Cache::set('module_list', $module_list);
        return $module_list;
    }

}