<?php
/**
 * Created by Shy
 * Date 2020/12/2
 * Time 13:37
 */


namespace app\admin\controller\v1;


use app\admin\model\SysModule;
use app\admin\model\SysRole;
use think\App;

class SysRbac extends BasicController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * 获取我的菜单
     * User: 万奇
     * Date: 2021/8/27 17:49
     * @throws \think\db\exception\DbException
     */
    public function get_menu_list(){
        $model          = new SysRole();
        $result         = $model->get_menu_list($this->user_info);

        response(200, '', $result);
    }

    /**
     * 获取角色列表
     * User: 万奇
     * Date: 2021/2/5 0005
     * @throws \think\db\exception\DbException
     */
    public function get_roles_list(){
        $model          = new SysRole();
        $result         = $model->get_roles_list($this->user_info);

        response(200, '', $result);
    }

    /**
     * 权限设置列表
     * User: 万奇
     * Date: 2021/2/5 0005
     * @throws \think\db\exception\DbException
     */
    public function show_roles_module(){
        $model          = new SysModule();
        $result         = $model->show_roles_module();

        response(200, '', $result);
    }

    /**
     * 新增角色
     * User: 万奇
     * Date: 2021/2/5 0005
     */
    public function roles_add(){
        param_receive(['name', 'module_id']);

        $this->validate($this->param, ['name' => 'max:50'], ['name.max' => '角色名称限50字符']);
        $model          = new SysRole();
        $model->roles_add($this->param, $this->user_info);

        response(200, '操作成功');
    }

    /**
     * 编辑角色
     * User: 万奇
     * Date: 2021/2/5 0005
     */
    public function roles_edit(){
        param_receive(['id', 'name', 'module_id', 'disable']);

        $this->validate($this->param, ['name' => 'max:50'], ['name.max' => '角色名称限50字符']);
        $model          = new SysRole();
        $model->roles_add($this->param, $this->user_info);

        response(200, '操作成功');
    }

    /**
     * 删除角色
     * User: 万奇
     * Date: 2021/2/5 0005
     */
    public function roles_del(){
        param_receive(['id']);

        $this->param['del']     = 1;
        $model          = new SysRole();
        $model->roles_add($this->param, $this->user_info);

        response(200, '操作成功');
    }

    /**
     * 获取角色信息
     * User: 万奇
     * Date: 2021/2/5 0005
     * @throws \think\db\exception\DbException
     */
    public function show_roles_info(){
        param_receive(['id']);

        $model          = new SysRole();
        $result         = $model->show_roles_info($this->param);

        response(200, '', $result);
    }
}