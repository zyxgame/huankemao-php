<?php
/**
 * User: 万奇
 * Date: 2021/7/30 18:38
 */

namespace app\api\model;


use app\core\Wechat;
use think\facade\Db;

class WxkStaff extends BasicModel
{
    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    /**
     * app 通过code获取成员信息
     * User: 万奇
     * Date: 2021/3/15 0015
     * @param $param
     * @return array|\think\Model|null
     * @throws \think\db\exception\DbException
     */
    public function get_user_info($param){
        $wechat             = new Wechat();
        $url                = 'https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo';
        $staff_user         = $wechat->request_wechat_api($url, 'wxk_app_secret', ['code' => $param['code']], false, false);

        if ($staff_user['errcode'] != 0){
            response(500, $staff_user['errmsg']);
        }

        $result                     = Db::name('wxk_staff')->field('user_id,name,mobile,qr_code')->where(['user_id' => $staff_user['UserId']])->find();
        $result['company_name']     = Db::name('wxk_department')->where(['parent_code' => 0])->value('name');

        return $result;
    }

}