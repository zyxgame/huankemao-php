<?php
/**
 * Created by PhpStorm.
 * User: 万奇
 * Date: 2021/3/26 0026
 * Time: 16:53
 */

namespace app\api\controller;


use app\core\Wechat;
use think\App;
use think\facade\Cache;
use think\facade\Db;

class CorpWeChat extends BasicController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * 获取JS_SDK
     * User: 万奇
     * Date: 2021/1/8 0008
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function get_js_sdk(){
        param_receive(['url']);
        $this->param['url'] = str_replace('&amp;', '&', $this->param['url']);

        $wechat             = new Wechat();
        $ticket             = Cache::get('app_jsapi_ticket');
        $api_ticket         = Cache::get('api_jsapi_ticket');

        // 应用
        if (!$ticket){
            $url                = 'https://qyapi.weixin.qq.com/cgi-bin/ticket/get';
            $jsapi_ticket       = $wechat->request_wechat_api($url, 'wxk_app_secret', ['type' => 'agent_config'], false, false);

            if ($jsapi_ticket['errcode'] != 0){
                response(500, $jsapi_ticket['errmsg']);
            }
            Cache::set('app_jsapi_ticket', $jsapi_ticket['ticket'], 7000);
            $ticket             = $jsapi_ticket['ticket'];
        }
        // 企业
        if (!$api_ticket){
            $url                = 'https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket';
            $get_jsapi_ticket   = $wechat->request_wechat_api($url, 'wxk_app_secret', [], false, false);

            if ($get_jsapi_ticket['errcode'] != 0){
                response(500, $get_jsapi_ticket['errmsg']);
            }
            Cache::set('api_jsapi_ticket', $get_jsapi_ticket['ticket'], 7000);
            $api_ticket             = $get_jsapi_ticket['ticket'];
        }

        $timestamp  = time();
        $nonceStr   = random_string();

        // 这里参数的顺序要按照 key 值 ASCII 码升序排序
        $string     = "jsapi_ticket=$ticket&noncestr=$nonceStr&timestamp=$timestamp&url={$this->param['url']}";
        $signature  = sha1($string);

        // 这里参数的顺序要按照 key 值 ASCII 码升序排序
        $api_string     = "jsapi_ticket=$api_ticket&noncestr=$nonceStr&timestamp=$timestamp&url={$this->param['url']}";
        $api_signature  = sha1($api_string);

        $config     = Db::name('wxk_config')->where(['wxk_id' => $this->param['corp_id']])->find();

        $result['agent']     = [
            'corpid'    => $config['wxk_id'],
            'agentid'   => $config['wxk_app_agent_id'],
            'ticket'    => $ticket,
            'nonceStr'  => $nonceStr,
            'timestamp' => $timestamp,
            'signature' => $signature,
        ];

        $result['config']     = [
            'appId'     => $config['wxk_id'],
            'ticket'    => $api_ticket,
            'nonceStr'  => $nonceStr,
            'timestamp' => $timestamp,
            'signature' => $api_signature,
        ];

        response(200, '', $result);
    }

}