<?php
/**
 * Created by PhpStorm.
 * User: 万奇
 * Date: 2021/3/12 0012
 * Time: 17:49
 */

namespace app\api\controller;

use think\App;

class Index extends BasicController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    public function index(){
        echo "hello api ~ index";die;
    }

    /**
     * 公共OSS上传图片
     * User: 万奇
     * Date: 2021/2/24 0024
     */
    public function upload_file_oss(){
        $result = upload_photo('file');

        response(200, '操作成功', $result);
    }

    /**
     * 首页概览
     * User: 万奇
     * Date: 2021/7/23 10:35
     * @throws \think\db\exception\DbException
     */
    public function home_overview(){
        param_receive(['start_time', 'end_time']);

        $model      = new \app\api\model\WxkCustomer();
        $result     = $model->home_overview($this->param);

        response(200, '', $result);
    }



}